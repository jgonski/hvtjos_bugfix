#!/bin/bash

#RunNumber=$1
#RDMSEED=$2

RunNumber=306784 # HVT VcXH
#RunNumber=450870 # HVT WZllqq
#RunNumber=450873 # RSG ZZllqq
#RunNumber=451145 # Radion ZZllqq
if [ $# -ge 1 ]; then
  RunNumber=$1
fi

doBugFix=1 ## fix the randseed bug?
if [ $# -ge 2 ]; then
  doBugFix=$2
fi

RDMSEED=124


## find the official JO
JO15=/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/
#JOBOPTNAME=`find ${JO15} -name "*${RunNumber}*.py" -print`
JOBOPTNAME="MC15.306784.MGPy8EG_A14NNPDF23LO_HVT_Agv1_VcXH_qqqq_m1000_m65.py"
JOBOPTPATH=`dirname ${JOBOPTNAME}`
JOBOPTNAME=`basename ${JOBOPTNAME}`
echo "JOBOBTNAME: ${JOBOPTNAME}"
#if [ "${JOBOPTNAME}X" == "X" ]; then
#  echo "JO not found for ${RunNumber}"
#  exit
#fi

JOtype=0 ## 0: HVT; 1: RSG; 2: Radion

## bug-fixed MadGraphControl file
if [[ "${JOBOPTNAME}" == *XH* ]]; then
  JOtype=0
  JOCTR=../MadGraph23Control_XH.py
elif [[ "${JOBOPTNAME}" == *RS_G* ]]; then
  JOtype=1
  JOCTR=../MadGraph23Control_BulkRS.py
elif [[ "${JOBOPTNAME}" == *radion* ]]; then
  JOtype=2
  JOCTR=../MadGraphControl_BulkRadion_fixed.py
fi

JOCTR=`readlink -f ${JOCTR}`
CTRNAME=`basename ${JOCTR}`

#export ATHENA_PROC_NUMBER=10

wdir=local_run_${RunNumber}_BugFix${doBugFix}
mkdir ${wdir}
cd ${wdir}


## to use the bug-fixed MadGraphControl file
if [ ${doBugFix} -eq 1 ]; then
  cp ${JOCTR} .
  echo "include(\"${CTRNAME}\")" > ${JOBOPTNAME}
fi 

## Athena setup for showering
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

if [ ${doBugFix} -eq 1 ]; then
  source $AtlasSetup/scripts/asetup.sh MCProd,20.7.9.9.27,slc6 # MadGraph(v.2.6.5)+Pythia8(v.240)
else
  if [ ${JOtype} -eq 2 ]; then  ## for radion
    source $AtlasSetup/scripts/asetup.sh MCProd,19.2.5.33.3,slc6 # 
  else
    source $AtlasSetup/scripts/asetup.sh AtlasProduction,19.2.5.14,slc6 # 
  fi
fi

## use official JO
if [ ${doBugFix} -eq 0 ]; then
  export DATAPATH=/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/exocontrol/:${DATAPATH}
  export JOBOPTSEARCHPATH=${JOBOPTPATH}:$JOBOPTSEARCHPATH
fi

Generate_tf.py --ecmEnergy 13000 \
   --runNumber ${RunNumber} --firstEvent 1 \
   --maxEvents 500 --randomSeed $RDMSEED \
   --jobConfig $JOBOPTNAME \
   --outputEVNTFile ${RunNumber}.evgen.root

cd -
