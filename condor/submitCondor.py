import numpy as np 
import os
import glob
import argparse

d_args ={
100000:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m1000_m300.py"  
,100001:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m1000_m500.py"  
,100002:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m1500_m400.py"
,100003:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m1500_m500.py"
,100004:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m1500_m600.py"
,100005:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m1500_m700.py"
,100006:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m1800_m500.py"
,100007:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m1800_m600.py"
,100008:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m1800_m700.py"
,100009:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m1800_m800.py"
,100010:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2000_m600.py"
,100011:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2000_m700.py"
,100012:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2000_m800.py"
,100013:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2000_m900.py"
,100014:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2000_m1000.py"
,100015:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2000_m1100.py"
,100016:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2300_m600.py"
,100017:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2300_m700.py"
,100018:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2300_m800.py"
,100019:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2300_m900.py"
,100020:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2300_m1000.py"
,100021:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2300_m1100.py"
,100022:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2300_m1200.py"
,100023:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2600_m700.py"
,100024:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2600_m800.py"
,100025:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2600_m900.py"
,100026:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2600_m1000.py"
,100027:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2600_m1100.py"
,100028:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2600_m1200.py"
,100029:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m2600_m1400.py"
,100030:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3000_m800.py"
,100031:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3000_m900.py"
,100032:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3000_m1000.py"
,100033:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3000_m1100.py"
,100034:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3000_m1200.py"
,100035:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3000_m1400.py"
,100036:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3000_m1600.py"
,100037:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3400_m900.py"
,100038:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3400_m1000.py"
,100039:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3400_m1100.py"
,100040:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3400_m1200.py"
,100041:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3400_m1400.py"
,100042:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3400_m1600.py"
,100043:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3600_m900.py"
,100044:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3600_m1000.py"
,100045:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3600_m1100.py"
,100046:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3600_m1200.py"
,100047:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3600_m1400.py"
,100048:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3600_m1600.py"
,100049:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3600_m1800.py"
,100050:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3800_m1000.py"
,100051:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3800_m1100.py"
,100052:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3800_m1200.py"
,100053:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3800_m1400.py"
,100054:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3800_m1600.py"
,100055:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3800_m1800.py"
,100056:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m3800_m2000.py"
,100057:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m4000_m1000.py"
,100058:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m4000_m1100.py"
,100059:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m4000_m1200.py"
,100060:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m4000_m1400.py"
,100061:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m4000_m1600.py"
,100062:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m4000_m1800.py"
,100063:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m4000_m2000.py"
,100064:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m4000_m2200.py"
,100065:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m5000_m200.py"
,100066:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m5000_m400.py"
,100067:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m5000_m600.py"
,100068:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m5000_m800.py"
,100069:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m5000_m1000.py"
,100070:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m5000_m1200.py"
,100071:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m5000_m1400.py"
,100072:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m5000_m1600.py"
,100073:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m5000_m1800.py"
,100074:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m5000_m2000.py"
,100075:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m5000_m2500.py"
,100076:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m6000_m200.py"
,100077:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m6000_m400.py"
,100078:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m6000_m600.py"
,100079:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m6000_m800.py"
,100080:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m6000_m1000.py"
,100081:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m6000_m1200.py"
,100082:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m6000_m1400.py"
,100083:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m6000_m1600.py"
,100084:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m6000_m1800.py"
,100085:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m6000_m2000.py"
,100086:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m6000_m2500.py"
,100087:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m6000_m3000.py"
,999999:"mc.MGPy8EG_A14NNPDF23LO_HVT_Agv_VcXH_qqqq_m1000_m300.py"
}

#-------------------------------------------------------------------------
if __name__ == "__main__":
   parser = argparse.ArgumentParser()

   transfer_input_files = "/data/users/jgonski/yXH/021021_REQUEST_EXT/"
   n_events = 10000

   for i in range(70,88):  #88
   #for i in range(1):  #88
     numName = '1000'
     if i < 10: numName += '0'+str(i)
     else: numName += str(i)
     #numName ='999999'

     # SUBMIT HERE
     print("SUBMITTING: input ", numName)
     args = open("args.txt","write")
     os.system("echo '"+numName+" "+str(n_events)+"' >>  args.txt")
     args.close()
     open("submit.sub","write")

     os.system("echo '#!/bin/bash' >> submit.sub")
     os.system("echo 'executable            = condor_evnt.sh' >> submit.sub") ##expand here to PC 
     os.system("echo 'output                = logs.$(ClusterId).$(ProcId).out' >> submit.sub")
     os.system("echo 'error                 = logs.$(ClusterId).$(ProcId).err' >> submit.sub")
     os.system("echo 'log                   = logs.$(ClusterId).log' >> submit.sub")
     os.system("echo 'universe         = vanilla' >> submit.sub")
     os.system("echo 'getenv           = True' >> submit.sub")
     os.system("echo 'Rank            = Mips' >> submit.sub")
     os.system("echo '' >> submit.sub")
     os.system("echo 'should_transfer_files = YES' >> submit.sub")
     os.system("echo 'when_to_transfer_output = ON_EXIT' >> submit.sub")
     os.system("echo 'initialdir = /data/users/jgonski/yXH/021021_REQUEST_EXT/condor/"+numName+"' >> submit.sub")
     #os.system("echo 'sampledir = /nevis/xenia/data/users/jgonski/xbb/Xbb_merged_samples/0121_PCJKDL1r' >> submit.sub")
     os.system("echo 'workdir = /data/users/jgonski/yXH/021021_REQUEST_EXT' >> submit.sub")
     os.system("echo 'transfer_input_files = $(workdir)/condor/condor_evnt.sh, $(workdir)/"+numName+"/"+d_args[int(numName)]+", $(workdir)/"+numName+"/MadGraphControl_HVT.py'  >> submit.sub")
     os.system("echo 'queue arguments from args.txt' >> submit.sub")

     os.system("condor_submit submit.sub")
     #time.sleep(.2)
     
     #open('submit.sub', 'w').close()

   print("DONE SUBMITTING... ")
