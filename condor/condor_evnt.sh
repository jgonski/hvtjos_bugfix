#!/bin/bash

echo "RUNNING..."

# Set up ATLAS on this machine:
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

export RUN_NUMBER=$1
export N_EVENTS=$2
export PWD=`pwd`
export STREND=".root"

mkdir $RUN_NUMBER
cp *.py $RUN_NUMBER

#1 make the EVNT
(
echo "GENERATING..."
asetup AthGeneration,21.6.59
Gen_tf.py --ecmEnergy=13000. --firstEvent=1  --maxEvents=$N_EVENTS --randomSeed=10041992 --jobConfig=$RUN_NUMBER --outputEVNTFile=$RUN_NUMBER$STREND > $RUN_NUMBER/log.generate
)

#2 make the TRUTH DAOD 
(
echo "GETTING TRUTH DAOD..."
asetup AthDerivation,21.2.115.0
Reco_tf.py --inputEVNTFile $RUN_NUMBER$STREND --outputDAODFile $RUN_NUMBER$STREND --reductionConf TRUTH3
)

#--------------------------------------------------------------
# Generate (already done)
#(
#asetup AthGeneration,21.6.48
#Gen_tf.py --ecmEnergy 13000 --firstEvent 1 --maxEvents $N_EVENTS --randomSeed 10041992 --jobConfig $RUN_NUMBER --outputEVNTFile EVNT.root
#)
#
## Reconstruct to Get Truth DAOD
#(
#echo "GETTING TRUTH DAOD..."
#asetup 21.2.86.0,AthDerivation
#Reco_tf.py --inputEVNTFile EVNT.root --outputDAODFile Hino.root --reductionConf TRUTH1
#)

