
# Issues to be fixed

Graviton and Radion samples seem to be affected by three things:
* hardcoded seeds
* Missing tau leptons in the definition of “l” for VBF RSG (GGF RSG samples have tau included)
* Missing b-quarks in the definition of “j”

HVT instead only has:
* hardcoded seeds
* Missing b-quarks in the definition of “j” (fixed only for ZH->qqbb according to the Jira)


# Notes from Dan

The original [MadGraphControl_HVT.py](https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/MadGraphControl_HVT.py)
The version with corrected j definition (used for samples where relevant) [MadGraph23Control_HVT.py](https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/MadGraph23Control_HVT.py).

The same for the Graviton:
[MadGraphControl_BulkRS.py](https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/MadGraphControl_BulkRS.py)
[MadGraph23Control_BulkRS.py](https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/MadGraph23Control_BulkRS.py)

The same for the Radion:
[MadGraphControl_BulkRadion.py](https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/MadGraphControl_BulkRadion.py)
[MadGraphControl_BulkRadion_fixed.py](https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/MadGraphControl_BulkRadion_fixed.py)

So the `j` definition is already fixed where relevant, and the random number seed is wrong everywhere. My first suggestion is that we make one new version of each signal type which fixes everything, so that all jobOptions use the same control file again. The drawback of that is that it would require some jobOptions to be updated, so for now we could just fix all control files separately and leave the jobOptions as they are (that would be the most straight forward and require no new DSID, etc). For tau leptons in the `l` definition for G and Radion I wasn't aware of this.

For particle definitions you need the proc cards which are:
[MadGraph23_proc_card_HVT](https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/exocontrol/MadGraph23_proc_card_HVT.dat)
[MadGraph23_proc_card_BulkRS](https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/exocontrol/MadGraph23_proc_card_BulkRS.dat)

Old [MadGraph_proc_card_BulkRS](https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/exocontrol/MadGraph_proc_card_BulkRS.dat) -> The jet definition is updated in new but the lepton definition looks correct (i.e. includes tau).
Old [MadGraph_proc_card_HVT](https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/exocontrol/MadGraph_proc_card_HVT.dat) -> just jet definition is updated in new file.

If i'm going to pull the trigger on my automated code for validation, etc, what I really need is a complete list of jobOptions (or at least DSID) that need to be re-run. For the combination I had already put this spreadsheet together, where the missing samples should have now been filled in, but would be good for people to check if anything is missing:

https://docs.google.com/spreadsheets/d/1YzKs6QKIEzsJjaUWmm8tLLnZ91SP_It7YJMz_PDABko/edit#gid=0

