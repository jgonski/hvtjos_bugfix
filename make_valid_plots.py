#!/usr/bin/env python

# Set up ROOT and RootCore:
print "Importing packages..."
from ROOT import *
from math import *
import os
import numpy as np
import sys
import argparse
import glob

colors=[kBlack,kRed,kRed+3,kOrange,kOrange+7,kGreen,kGreen+3,kCyan,kBlue,kBlue+3,kViolet,kPink+10,kMagenta+2,kGray+1]
#colors=[kRed,kOrange,kGreen,kBlue,kViolet]

#--------------------------------------------------------------
def check_masses(fs):
  YNum = 34
  for f in fs: 
    YNum = 9000002 if '21632' in f else 34
    xAOD.Init()
    FF = TFile(f, 'READ')
    TT = FF.Get("CollectionTree")
    TT.Draw("TruthBSMWithDecayParticlesAuxDyn.m>>h","TruthBSMWithDecayParticlesAuxDyn.pdgId==6666 || TruthBSMWithDecayParticlesAuxDyn.pdgId==-6666")
    hAfter = gDirectory.Get("h")
    mean = int(np.divide(hAfter.GetMean(),1000))
    if not mean %10 == 0: mean +=1

    TT.Draw("TruthBSMWithDecayParticlesAuxDyn.m>>h1","TruthBSMWithDecayParticlesAuxDyn.pdgId=="+str(YNum)+" || TruthBSMWithDecayParticlesAuxDyn.pdgId==-"+str(YNum))
    hAfter1 = gDirectory.Get("h1")
    mean1 = int(np.divide(hAfter1.GetMean(),1000))
    if not mean1 %10 == 0: mean1 +=1
    print('X: ', mean, ' Y: ', mean1)

#--------------------------------------------------------------
def get_bins(var,p):
  bins = []
  if 'parent' in var: bins=[100, -50, 50]
  elif p == 6666 and 'child' in var: bins= [10, -5,5]
  elif p == 34 and 'child' in var: bins=[14000, -7000, 7000]
  elif 'nChild' in var: bins=[15, 0, 15]
  elif 'pt' in var: bins=[50,0,2000000]
  elif 'm' in var: bins=[61,0,6100000]

  return bins

#--------------------------------------------------------------
def stylize(h,i,partStr,var):
  h.SetTitle(partStr+" "+str(var))
  h.GetXaxis().SetTitle(partStr+' '+str(var)+' [GeV]')
  h.SetLineColor(colors[i]) 
  h.SetLineWidth(2)
  h.Scale(1/h.Integral()) 
  h.GetYaxis().SetRangeUser(0.0005,100)

#--------------------------------------------------------------
def make_hist(fs,var, p,numEntries,saveName):
  c1 = TCanvas('c1', 'c1', 500,400)
  c1.SetLogy()
  gStyle.SetOptStat(0)
  hs=[]
  if p==34: partStr = 'Y' 
  if p==6666: partStr = 'X' 

  varNames = ['pt','m','nChild','parent','child']
  for f in fs:
    hs.append(getVar(f, var,p,numEntries))
  print(np.shape(hs))

  if var=='all':
    for i in range(5):
      leg = TLegend(0.2,0.6,0.88,0.93)
      leg.SetTextSize(0.038)
      for h in range(len(fs)):
        output = os.popen("grep \""+fs[h].split(".")[1].replace("valid_","")+"\" dsid_dict.dat").read()
        stylize(hs[h][i],h,partStr,varNames[i])
        leg.AddEntry(hs[h][i], output+" ("+str(hs[h][i].GetEntries())+")","l")
        if h==0: hs[h][i].Draw("hist")
        else: hs[h][i].Draw("hist+same")
      leg.Draw("same")

      c1.SaveAs("plots/"+saveName+"_"+partStr+"_"+varNames[i]+".png")
    
  
  else:
    leg = TLegend(0.2,0.6,0.88,0.93)
    leg.SetTextSize(0.038)
    for h in range(len(hs)):
      output = os.popen("grep \""+fs[h].split(".")[1].replace("valid_","")+"\" dsid_dict.dat").read()
      print(output)
      stylize(hs[h],h,partStr,var)
      leg.AddEntry(hs[h], output+" ("+str(hs[h].GetEntries())+")","l")
      if h==0: hs[h].Draw("hist")
      else: hs[h].Draw("hist+same")
    leg.Draw("same")

    c1.SaveAs("plots/"+saveName+"_"+partStr+"_"+str(var)+".png")


#--------------------------------------------------------------
def getVar(f,var,p,numEntries):
  f1 = TFile(f, 'READ' )
  if p==34 and '21632' in f: p1 = 9000002
  else:
    p1 = p
  bins=get_bins(var,p)
  name=f.split(".")[1].replace("valid_","")+"_"+var+"_"+str(p)
  print('getting var ', var)
  print('for ',name,' p = ',p1)
  
  # make hists  
  if var == 'all': 
    h1 = TH1F(name+"_pt"      ,name+"_pt"      ,50,0,2000000)
    h2 = TH1F(name+"_m"       ,name+"_m"       ,61,0,6100000)
    h3 = TH1F(name+"_nChild"  ,name+"_nChild"  ,15, 0, 15)
    h4 = TH1F(name+"_parent"  ,name+"_parent"  ,100, -50, 50)
    if p==6666: h5 = TH1F(name+"_children",name+"_children",10, -5,5)
    else: h5 = TH1F(name+"_children",name+"_children",14000, -7000, 7000)
  else: 
    h1 = TH1F(name,name,bins[0],bins[1],bins[2])


  xAOD.Init()
  t1 = xAOD.MakeTransientTree( f1 )
  #t1 = f1.Get("CollectionTree")

  # loop over file
  for j in range(numEntries):
     nb = t1.GetEntry( j )
     if nb <= 0: continue
     allTruthBSMWithDecayParticles = t1.TruthBSMWithDecayParticles
     for part in allTruthBSMWithDecayParticles:
       if abs(part.pdgId()) == p1: 
         #fill just one var 
         if var == 'all':
           h1.Fill(part.pt())
           h2.Fill(part.m())
           h3.Fill(part.nChildren())
           if part.nChildren() >=2:
             if part.parent().pdgId() != p: h4.Fill(abs(part.parent().pdgId()))
             h5.Fill(part.child(0).pdgId())
             h5.Fill(part.child(1).pdgId())
             if p == 34: 
               print('child 0 of ', str(part.nChildren()),'; pdgId: ', part.child(0).pdgId())
               print('child 1 of ', str(part.nChildren()),'; pdgId: ', part.child(1).pdgId())
         else:
           if part.nChildren() >=2:
             if 'parent' in var: 
               if part.parent().pdgId() != p: h1.Fill(abs(part.parent().pdgId()))
             elif 'child' in var:
               #print('child 0 of ', str(part.nChildren()),'; pdgId: ', part.child(0).pdgId())
               #print('child 1 of ', str(part.nChildren()),'; pdgId: ', part.child(1).pdgId())
               h1.Fill(part.child(0).pdgId())
               h1.Fill(part.child(1).pdgId())
             elif 'nC' in var: h1.Fill(part.nChildren())
             elif 'pt' in var: h1.Fill(part.pt())
             elif 'm' in var: h1.Fill(part.m())

  print('Number of entries: ' , h1.GetEntries())
  if var == 'all': return [h1,h2,h3,h4,h5]
  else: return [h1]



#--------------------------------------------------------------
def main():

  parser = argparse.ArgumentParser()
  parser.add_argument("-p", "--particle", default = '', type=str, nargs='+',
                     help="PDGID to plot")
  parser.add_argument("-v", "--variable", default = '', type=str, nargs='+',
                     help="Var to plot")
  parser.add_argument("-f", "--files", default = '', type=str, nargs='+',
                     help="file list string")
  parser.add_argument("-n", "--events", default = 1000, type=int, nargs='+',
                     help="number of events")
  parser.add_argument("-s", "--save", default = 'test', type=str, nargs='+',
                     help="save name")
  args = parser.parse_args()
  p = int(args.particle[0])
  var = args.variable[0]
  fList = args.files[0]
  if args.events: numEntries = int(args.events[0])
  else: numEntries = 1000
  saveName = args.save[0]

  #---- TRUTH1 fs 
  fs = glob.glob(fList)
  print('running over ', fs)

  #check_masses(fs)
  make_hist(fs, var, p, numEntries,saveName)




#--------------------------------------------------------------
if __name__ == '__main__':
  main()

