#!/usr/bin/env python

# Set up ROOT and RootCore:
print "Importing packages..."
from ROOT import *
from math import *
import numpy as np
import sys
import argparse
import glob

colors=[kBlack,kRed,kRed+3,kOrange,kOrange+7,kGreen,kGreen+3,kTeal-1,kCyan,kBlue,kBlue+3,kViolet,kPink+10,kMagenta+2,kGray+1]

#--------------------------------------------------------------
def make_comp(fs,var, p):
  c1.SetLogy()
  gStyle.SetOptStat(0)
  hpt1,hpt2,hpt3,hpt4,hpt5 = var(fs, p)
  hpt1.SetLineColor(colors[0]) 
  hpt2.SetLineColor(colors[1]) 
  hpt3.SetLineColor(colors[2]) 
  hpt4.SetLineColor(colors[3]) 
  hpt5.SetLineColor(colors[4]) 
  hpt1.SetLineWidth(2)
  hpt2.SetLineWidth(2)
  hpt3.SetLineWidth(2)
  hpt4.SetLineWidth(2)
  hpt5.SetLineWidth(2)
  hpt1.Draw('hist')
  hpt2.Draw('hist+same')
  hpt3.Draw('hist+same')
  hpt4.Draw('hist+same')
  hpt5.Draw('hist+same')
  hpt1.Draw('hist+same')
  leg = TLegend(0.3,0.65,0.88,0.89)
  leg.SetTextSize(0.038)
  leg.AddEntry(hpt1, fs[0].split(".")[1].replace("valid_","")+" ("+str(hpt1.GetEntries())+")","l")
  leg.AddEntry(hpt2, fs[1].split(".")[1].replace("valid_","")+" ("+str(hpt2.GetEntries())+")","l")
  leg.AddEntry(hpt3, fs[2].split(".")[1].replace("valid_","")+" ("+str(hpt3.GetEntries())+")","l")
  leg.AddEntry(hpt4, fs[3].split(".")[1].replace("valid_","")+" ("+str(hpt4.GetEntries())+")","l")
  leg.AddEntry(hpt5, fs[4].split(".")[1].replace("valid_","")+" ("+str(hpt5.GetEntries())+")","l")
  leg.Draw("same")
  if p==34: 
    hpt1.SetTitle("Y pT")
    hpt1.GetXaxis().SetTitle('Y pT [GeV]')
    c1.SaveAs("plots/0215_y_pt.pdf")
  if p==6666: 
    hpt1.SetTitle("X pT")
    hpt1.GetXaxis().SetTitle('X pT [GeV]')
    c1.SaveAs("plots/0215_x_pt.pdf")
  #hpt1.GetYaxis().SetRangeUser(0,85)

#--------------------------------------------------------------
def fill_hist(t1,var):

  if 'm' in var: h1 = TH1F('hpt1','hpt1', 125,0,2500000)
  elif 'pt' in var: h1 = TH1F('hpt1','hpt1', 125,0,2500000)
  for j in range(t1.GetEntries()):
     nb = t1.GetEntry( j )
     if nb <= 0: continue
     allTruthBSMWithDecayParticles = t1.TruthBSMWithDecayParticles
     for part in allTruthBSMWithDecayParticles:
       if abs(part.pdgId()) == p: h1.Fill(part.pt())

  return h
#--------------------------------------------------------------
def var(fs,p):
  f1 = TFile(fs[0], 'READ' )
  f2 = TFile(fs[1], 'READ' )
  f3 = TFile(fs[2], 'READ' )
  f4 = TFile(fs[3], 'READ' )
  f5 = TFile(fs[4], 'READ' )

  if 'm' in var:
    h1 = TH1F('hpt1','hpt1', 50,0,2000000)
    h2 = TH1F('hpt2','hpt2', 50,0,2000000)
    h3 = TH1F('hpt3','hpt3', 50,0,2000000)
    h4 = TH1F('hpt4','hpt4', 50,0,2000000)
    h5 = TH1F('hpt5','hpt5', 50,0,2000000)
  elif 'pt' in var:
    h1 = TH1F('hpt1','hpt1', 50,0,2000000)
    h2 = TH1F('hpt2','hpt2', 50,0,2000000)
    h3 = TH1F('hpt3','hpt3', 50,0,2000000)
    h4 = TH1F('hpt4','hpt4', 50,0,2000000)
    h5 = TH1F('hpt5','hpt5', 50,0,2000000)

  xAOD.Init()
  t1 = xAOD.MakeTransientTree( f1 )
  t2 = xAOD.MakeTransientTree( f2 )
  t3 = xAOD.MakeTransientTree( f3 )
  t4 = xAOD.MakeTransientTree( f4 )
  t5 = xAOD.MakeTransientTree( f5 )

  for j in range(t1.GetEntries()):
     nb = t1.GetEntry( j )
     if nb <= 0: continue
     allTruthBSMWithDecayParticles = t1.TruthBSMWithDecayParticles
     for part in allTruthBSMWithDecayParticles:
       if abs(part.pdgId()) == p: h1.Fill(part.pt())
  for j in range(t2.GetEntries()):
     nb = t2.GetEntry( j )
     if nb <= 0: continue
     allTruthBSMWithDecayParticles = t2.TruthBSMWithDecayParticles
     for part in allTruthBSMWithDecayParticles:
       if abs(part.pdgId()) == p: h2.Fill(part.pt())
  for j in range(t3.GetEntries()):
     nb = t3.GetEntry( j )
     if nb <= 0: continue
     allTruthBSMWithDecayParticles = t3.TruthBSMWithDecayParticles
     for part in allTruthBSMWithDecayParticles:
       if abs(part.pdgId()) == p: h3.Fill(part.pt())
  for j in range(t4.GetEntries()):
     nb = t4.GetEntry( j )
     if nb <= 0: continue
     allTruthBSMWithDecayParticles = t4.TruthBSMWithDecayParticles
     for part in allTruthBSMWithDecayParticles:
       if abs(part.pdgId()) == p: h4.Fill(part.pt())
  for j in range(t5.GetEntries()):
     nb = t5.GetEntry( j )
     if nb <= 0: continue
     allTruthBSMWithDecayParticles = t5.TruthBSMWithDecayParticles
     for part in allTruthBSMWithDecayParticles:
       if abs(part.pdgId()) == p: h5.Fill(part.pt())


  return [h1,h2,h3,h4,h5]

#--------------------------------------------------------------
def relatives(fs,relative,p):

  if 'parent' in relative: h  = TH1F('hp1','hp1', 1000, 0, 11000000)
  #if 'parent' in relative: h  = TH1F('hp1','hp1',100,-50,50)
  if abs(p) == 6666 and 'child' in relative: h = TH1F('hc1','hc1', 10, -5,5)
  if abs(p) == 34 and 'child' in relative: h = TH1F('hc1','hc1', 14000, -7000, 7000)
  #if 'child' in relative: h = TH1F('hc1','hc1', 10, -5, 5)
  if 'nChild' in relative: h = TH1F('hn1','hn1', 15, 0, 15)
  if 'pt' in relative: h = TH1F('hpt','hpt', 1000,0,10000000)

  xAOD.Init()
  ts = []
  for i in range(len(fs)):
    ts.append(xAOD.MakeTransientTree(TFile(fs[i], 'READ' ) ))
 

  for t in ts:
    print('starting ',t)
    for j in range(t.GetEntries()):
       nb = t.GetEntry( j )
       if nb <= 0: continue
       allTruthBSMWithDecayParticles = t.TruthBSMWithDecayParticles
       for part in allTruthBSMWithDecayParticles:
         if abs(part.pdgId()) == p: 
           if part.nChildren() >=2:
             if 'parent' in relative: 
               if part.parent().pdgId() != p: h.Fill(abs(part.parent().pdgId()))
             elif 'child' in relative:
               print('child 0 of ', str(part.nChildren()),'; pdgId: ', part.child(0).pdgId())
               print('child 1 of ', str(part.nChildren()),'; pdgId: ', part.child(1).pdgId())
               h.Fill(part.child(0).pdgId())
               h.Fill(part.child(1).pdgId())
             elif 'nC' in relative: h.Fill(part.nChildren())
             elif 'pt' in relative: h.Fill(part.pt())

  print('num entries: ', h.GetEntries())
  return h
         

#--------------------------------------------------------------
def get_trees(fs):
  ts = []
  for f in fs:
    ff = TFile(f, 'READ' )
    ts.append(ff.Get("CollectionTree"))
  return ts

#--------------------------------------------------------------
def run(var, part, fs):
  print('run ', var, part,fs)

  # Initialize the xAOD infrastructure
  xAOD.Init()
  c1 = TCanvas('c1', 'c1', 500,400)
  c1.SetLogy()
  gStyle.SetOptStat(0)
  leg = TLegend(0.3,0.65,0.88,0.89)
  leg.SetTextSize(0.038)
  upperBound = 6500000
  if part==6666: upperBound = 3500000

  ts = []
  hs = []
  for i in range(len(fs)):
    hBefore = TH1F('h','h', 1000, 0, upperBound) 
    print(fs[i])
    FF = TFile(fs[i], 'READ')
    TT = FF.Get("CollectionTree")
    print(TT.GetEntries())
    TT.Draw("TruthBSMWithDecayParticlesAuxDyn."+var+">>h","TruthBSMWithDecayParticlesAuxDyn.pdgId=="+str(part)+" || TruthBSMWithDecayParticlesAuxDyn.pdgId==-"+str(part))
    hAfter = gDirectory.Get("h")
    #ts[i].Draw("TruthBSMWithDecayParticlesAuxDyn."+var+">>h"+str(i),"TruthBSMWithDecayParticlesAuxDyn.pdgId=="+str(part)+" || TruthBSMWithDecayParticlesAuxDyn.pdgId==-"+str(part))
    #TT.Draw("TruthBSMWithDecayParticlesAuxDyn.m>>h"+str(i))
    #hs.append(hAfter)
    hAfter.SetLineColor(colors[i])
    hAfter.GetYaxis().SetRangeUser(0.1,hAfter.GetEntries()*300)
    hAfter.GetXaxis().SetRangeUser(0,upperBound)
    leg.AddEntry(hAfter, fs[i].split(".")[1].replace("valid_","")+" ("+str(hAfter.GetEntries())+")","l")
    if i == 0: 
      if 'm' in var: hAfter.GetXaxis().SetTitle('Truth BSM mass [GeV]')
      else: hAfter.GetXaxis().SetTitle(var)
      if part==6666: hAfter.SetTitle("X (6666)")
      elif part==34: hAfter.SetTitle("Y (+-34)")
      else: hAfter.SetTitle(str(part))
      hAfter.Draw("hist")
    else: hAfter.Draw("hist+same")
    hAfter.GetYaxis().SetRangeUser(0.1,hAfter.GetEntries()*300)
    hAfter.GetXaxis().SetRangeUser(0,upperBound)

  #for h in range(len(hs)):
  #  hs[h].SetLineColor(colors[h])
  #  hs[h].GetYaxis().SetRangeUser(0.1,hs[0].GetEntries()*300)
  #  hs[h].GetXaxis().SetRangeUser(0,upperBound)
  #  leg.AddEntry(hs[h], fs[h].split(".")[1].replace("valid_","")+" ("+str(hs[h].GetEntries())+")","l")

 
  #hs[0].Draw("hist")
  #for y in range(1,len(hs)-1):
  #  hs[y].Draw('hist+same')
  leg.SetBorderSize(0)
  leg.SetNColumns(1)
  leg.Draw("same")
  c1.SaveAs("plots/0215_"+var+"_"+str(part)+".pdf")


  # Loop over events 
  #for entry in xrange( t.GetEntries() ):
  #  t.GetEntry( entry )
  #  print 'Processing Entry', entry, 'of ', t.GetEntries()

  #  # Look at jets 
  #  #for tp in t.AntiKt10TruthTrimmedPtFrac5SmallR20Jets:
  #  #  print(tp.D2)
  #  #for tp in t.TruthBSMWithDecayParticlesAuxDyn:
  #  #  print(tp.pdgId())

  #  #if ph.auxdataConst("char")("DFCommonPhotonsIsEMLoose") == '\0':

#--------------------------------------------------------------
def main():

  #parser = argparse.ArgumentParser()
  #parser.add_argument("-p", "--particle", default = [], type=str, nargs='+',
  #                   help="Truth DAOD to run over")
  #parser.add_argument("-v", "--variable", default = [], type=str, nargs='+',
  #                   help="Truth DAOD to run over")
  #args = parser.parse_args()
  #part = args.particle[0]
  #var = args.variable[0]

  #---- TRUTH1 fs = ["DAOD_TRUTH1.final_306808_m1500_m130.test.pool.root","DAOD_TRUTH1.final_306834_m2600_m200.test.pool.root","DAOD_TRUTH1.final_306878_m3000_m700.test.pool.root","DAOD_TRUTH1.final_306886_m4000_m900.test.pool.root"]
  fs = glob.glob("condor/DAOD_TRUTH3.valid_m1*.test.pool.root")
  fs = ["condor/DAOD_TRUTH3.valid_m1000_m500.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m1500_m700.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m1800_m800.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m2000_m1100.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m2300_m1200.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m2600_m1400.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m3000_m1600.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m3400_m1600.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m3600_m1800.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m3800_m2000.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m4000_m2200.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m5000_m2500.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m6000_m3000.test.pool.root"
  ]
  fs=[
  "condor/DAOD_TRUTH3.valid_m5000_m600.test.pool.root"
  ,"condor/DAOD_TRUTH3.valid_m6000_m600.test.pool.root"
  ]
  #fs = ["condor/DAOD_TRUTH3.valid_m6000_m3000.test.pool.root"]
  #print("Plot of ", var,  "for ", part)
  #run(var, part,fs)

  c1 = TCanvas('c1', 'c1', 500,400)
  
  #--- Parent
  #hp1 = relatives(fs,'parent', 6666)
  #hp1.Draw('hist')
  #hp1.SetTitle("X Parents")
  #hp1.GetXaxis().SetTitle('X parent [pdgId]')
  #c1.SaveAs("plots/0215_x_parents.pdf")
  ##---- Children
  #hc1 = relatives(fs,'child',6666)
  #hc1.Draw('hist')
  #hc1.SetTitle("X Children")
  #hc1.GetXaxis().SetTitle('X children [pdgId]')
  #c1.SaveAs("plots/0215_x_children.pdf")
  #hc1 = relatives(fs,'child',34)
  #hc1.Draw('hist')
  #hc1.SetTitle("Y Children")
  #hc1.GetXaxis().SetTitle('Y children [pdgId]')
  #c1.SaveAs("plots/0215_y_children.pdf")
  ##----- NChild 
  #hn1 = relatives(fs,'nChild',34)
  #hn1.Draw('hist')
  #hn1.SetTitle("Y Number of Children")
  #hn1.GetXaxis().SetTitle('Number of Y children')
  #c1.SaveAs("plots/0215_y_nChild.pdf")

  #--- Pt
  #make_comp(fs,'m',34)
  #make_comp(fs, 'm',6666)
  run('m',34,fs)
  run('m',6666,fs)




#--------------------------------------------------------------
if __name__ == '__main__':
  main()

